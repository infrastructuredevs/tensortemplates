ActiveThorns = "
    thc_grsource_orig
    thc_grsource_templates
    thc_grsource_comparison
    ADMBase
    ADMCoupling
    ADMMacros
    AEILocalInterp
    BNSTrackerGen
    Boundary
    Carpet
    CarpetInterp
    CarpetInterp2
    CarpetIOASCII
    CarpetIOBasic
    CarpetIOHDF5
    CarpetIOScalar
    CarpetLib
    CarpetRegrid2
    CarpetReduce
    CarpetSlab
    CartGrid3D
    Constants
    CoordBase
    CoordGauge
    Dissipation
    Formaline
    EOS_Barotropic
    EOS_Thermal
    EOS_Thermal_Extable
    EOS_Thermal_Table3d
    Fortran
    GenericFD
    HydroBase
    HRSCCore
    ID_Switch_EOS
    InitBase
    IOUtil
    LocalInterp2
    LoopControl
    ML_ADMConstraints
    ML_CCZ4
    ML_CCZ4_Helper
    MoL
    NaNChecker
    NewRad
    PizzaBase
    PizzaIDBase
    PizzaNumUtils
    PizzaTOV
    ReflectionSymmetry
    Refluxing
    Slab
    SpaceMask
    SphericalHarmonicDecomp
    SphericalSurface
    StaticConformal
    SymBase
    SystemStatistics
    TerminationTrigger
    THC_Analysis
    THC_Core
    THC_Refluxing
    THC_Tracer
    Time
    TimerReport
    TmunuBase
    Volomnia
    WatchDog
"

Cactus::terminate			= "iteration"
Cactus::cctk_itlast = 256

Formaline::output_source_subdirectory		= "../../cactus-source"

#TerminationTrigger::max_walltime		= @WALLTIME_HOURS@
#TerminationTrigger::on_remaining_walltime	= 30
TerminationTrigger::create_termination_file	= "yes"
TerminationTrigger::termination_from_file	= "yes"
TerminationTrigger::termination_file		= "../TERMINATE"

TimerReport::out_every		= 8192
TimerReport::output_all_timers_readable		= "yes"

# =============================================================================
# Grid
# =============================================================================
Grid::avoid_origin			= "no"
Grid::domain			= "full"
Grid::type			= "coordbase"

ReflectionSymmetry::reflection_x		= "yes"
ReflectionSymmetry::reflection_y		= "no"
ReflectionSymmetry::reflection_z		= "yes"
ReflectionSymmetry::avoid_origin_x		= "yes"
ReflectionSymmetry::avoid_origin_y		= "yes"
ReflectionSymmetry::avoid_origin_z		= "yes"

Volomnia::symm_weight		= 2

CoordBase::xmin			= -512
CoordBase::xmax			=  512
CoordBase::ymin			= -512
CoordBase::ymax			=  512
CoordBase::zmin			=  0
CoordBase::zmax			=  512

CoordBase::spacing			= "numcells"
CoordBase::ncells_x			= 110
CoordBase::ncells_y			= 110
CoordBase::ncells_z			= 55

CoordBase::boundary_size_x_lower		= 3
CoordBase::boundary_size_x_upper		= 3
CoordBase::boundary_shiftout_x_lower		= 0
CoordBase::boundary_shiftout_x_upper		= 0
CoordBase::boundary_staggered_x_lower		= "yes"
CoordBase::boundary_staggered_x_upper		= "yes"

CoordBase::boundary_size_y_lower		= 3
CoordBase::boundary_size_y_upper		= 3
CoordBase::boundary_shiftout_y_lower		= 0
CoordBase::boundary_shiftout_y_upper		= 0
CoordBase::boundary_staggered_y_lower		= "yes"
CoordBase::boundary_staggered_y_upper		= "yes"

CoordBase::boundary_size_z_lower		= 3
CoordBase::boundary_size_z_upper		= 3
CoordBase::boundary_shiftout_z_lower		= 0
CoordBase::boundary_shiftout_z_upper		= 0
CoordBase::boundary_staggered_z_lower		= "yes"
CoordBase::boundary_staggered_z_upper		= "yes"

Driver::ghost_size			= 3
Driver::ghost_size_x		= 3
Driver::ghost_size_y		= 3
Driver::ghost_size_z		= 3

Carpet::refinement_centering    = "cell"
Carpet::domain_from_coordbase		= "yes"

InitBase::initial_data_setup_method		= "init_some_levels"

# See https://trac.einsteintoolkit.org/ticket/1512
LoopControl::settle_after_iteration		= 0

Carpet::max_refinement_levels		= 7
Carpet::prolongation_order_space		= 3
Carpet::prolongation_order_time		= 2
Carpet::use_buffer_zones		= "yes"
Carpet::enable_all_storage		= "no"
Carpet::init_fill_timelevels		= "yes"

Carpet::grid_coordinates_filename		= "grid.carpet"

CarpetLib::poison_new_memory		= "yes"

CarpetRegrid2::num_centres		= 3
CarpetRegrid2::regrid_every		= 64
CarpetRegrid2::snap_to_coarse           = "yes"
CarpetRegrid2::freeze_unaligned_levels          = "yes"
CarpetRegrid2::freeze_unaligned_parent_levels   = "yes"

# ----------- Region 1 --------------------
CarpetRegrid2::active_1		= "yes"
CarpetRegrid2::num_levels_1		= 6
CarpetRegrid2::position_x_1 = 38.75
CarpetRegrid2::position_y_1 = -31.5980616494
CarpetRegrid2::radius_1[1]		= 192.0
CarpetRegrid2::radius_1[2]		= 96.0
CarpetRegrid2::radius_1[3]		= 48.0
CarpetRegrid2::radius_1[4]		= 24.0
CarpetRegrid2::radius_1[5]		= 16.0

# ----------- Region 2 --------------------
CarpetRegrid2::active_2		= "yes"
CarpetRegrid2::num_levels_2		= 6
CarpetRegrid2::position_x_2 = -38.75
CarpetRegrid2::position_y_2 = 31.5980616494
CarpetRegrid2::radius_2[1]		= 192.0
CarpetRegrid2::radius_2[2]		= 96.0
CarpetRegrid2::radius_2[3]		= 48.0
CarpetRegrid2::radius_2[4]		= 24.0
CarpetRegrid2::radius_2[5]		= 16.0

# ----------- Region 3 --------------------
CarpetRegrid2::active_3		= "yes"
CarpetRegrid2::num_levels_3		= 3
CarpetRegrid2::position_x_3		= 0

CarpetRegrid2::radius_3[1]		= 256.0
CarpetRegrid2::radius_3[2]		= 128.0

CarpetRegrid2::radius_3[3]		= 64.0
CarpetRegrid2::radius_3[4]		= 32.0

CarpetRegrid2::radius_x_3[5]		= 24.0
CarpetRegrid2::radius_y_3[5]		= 24.0
CarpetRegrid2::radius_z_3[5]		= 16.0

CarpetRegrid2::radius_3[6]		= 8.0
# -----------------------------------------

BNSTrackerGen::sym_pi		= "no"
BNSTrackerGen::analysis_reflevel		= 5
BNSTrackerGen::analyze_every		= 64
BNSTrackerGen::merge_separation		= 1.5
BNSTrackerGen::collapse_separation		= -1
BNSTrackerGen::add_levels_post_merge		= 3
BNSTrackerGen::add_levels_post_collapse		= 0

CarpetRegrid2::symmetry_rotating180		= "no"
CarpetRegrid2::verbose		= "no"

NaNChecker::check_every		= 128
NaNChecker::check_vars		= "
    HydroBase::rho
    HydroBase::vel
    HydroBase::w_lorentz
    THC_Core::densxn
    THC_Core::densxp
    THC_Core::scon
    THC_Core::tau
    THC_Core::volform
"
NaNChecker::action_if_found		= "terminate"

# =============================================================================
# Time integration
# =============================================================================
Carpet::num_integrator_substeps		= 3

MoL::ode_method			= "RK3"
MoL::MoL_Intermediate_Steps		= 3
MoL::MoL_Num_Scratch_Levels		= 0
MoL::verbose			= "register"

HydroBase::timelevels		= 3

Time::timestep_method		= "courant_static"
Time::dtfac			= 0.15

# =============================================================================
# Initial data
# =============================================================================
ADMBase::initial_data		= "PizzaTOV"
ADMBase::initial_lapse		= "PizzaTOV"
ADMBase::initial_shift		= "PizzaTOV"
ADMBase::initial_dtlapse		= "zero"
ADMBase::initial_dtshift		= "zero"
HydroBase::initial_hydro		= "PizzaTOV"
HydroBase::initial_entropy		= "THCode"
HydroBase::initial_temperature		= "PizzaTOV"
HydroBase::initial_Y_e		= "PizzaTOV"
HydroBase::initial_Abar		= "zero"

# File describing a one-parametric EOS in Pizza format. Used only for initial data.
PizzaIDBase::eos_file		= "/home/astro/shared/projekt/EOS_Collection/LS220/old/0.01MeVbeta/LS220B0.pizza"
#PizzaIDBase::eos_file		= "/gpfs/work/pr62do/di25def2/initial/eos/DD2/0.01MeV/eos_dd2_adb.pizza"

# q = 1.0
PizzaTOV::idtype = "BNS"
# m_0 = 1.350037233
PizzaTOV::star_rhoc[0] = 5.68317888184e+17
PizzaTOV::star_posx[0] = 38.75
PizzaTOV::star_posy[0] = -31.5980616494
PizzaTOV::star_velx[0] = -0.1
PizzaTOV::star_vely[0] = 0
# m_1 = 1.350037233
PizzaTOV::star_rhoc[1] = 5.68317888184e+17
PizzaTOV::star_posx[1] = -38.75
PizzaTOV::star_posy[1] = 31.5980616494
PizzaTOV::star_velx[1] = 0.1
PizzaTOV::star_vely[1] = 0

# Geometric unit system for initial data, specified by length unit.
# use CACTUS units
PizzaBase::length_unit		= 1476.7161818921163

# Switch EOS
ID_Switch_EOS::sync_eps_temp		= "yes"
ID_Switch_EOS::temp_from_eps		= "no"
ID_Switch_EOS::limit_efrac		= "yes"

# =============================================================================
# Templated hydrodynamics code
# =============================================================================
HydroBase::evolution_method		= "THCode"
HydroBase::temperature_evolution_method		= "THCode"
HydroBase::entropy_evolution_method		= "THCode"
HydroBase::Y_e_evolution_method		= "THCode"

THC_Core::eos_type			= "nuclear"
THC_Core::physics			= "GRHD"

TmunuBase::prolongation_type		= "none"
TmunuBase::stress_energy_storage		= "yes"
TmunuBase::stress_energy_at_RHS		= "yes"
TmunuBase::support_old_CalcTmunu_mechanism	= "no"

THC_Core::bc_type			= "none"

HRSCCore::scheme			= "FV"
HRSCCore::pplim			= "yes"
HRSCCore::reconstruction		= "MP5"
HRSCCore::riemann_solver		= "HLLE"
HRSCCore::refluxing         = "yes"
HRSCCore::flux_split		= "LLF"
HRSCCore::system_split		= "components"
HRSCCore::speed_eps			= 0.05

THC_Core::atmo_rho			= 1e-14
THC_Core::atmo_tolerance		= 0.0
THC_Core::atmo_temperature		= 0.02

THC_Core::c2a_BH_alp		= 0.15
THC_Core::c2a_rho_strict		= 2e-5
THC_Core::c2a_set_to_nan_on_failure		= "no"
THC_Core::c2a_fix_conservatives		= "yes"
THC_Core::c2a_kill_on_failure		= "no"

THC_Refluxing::nvars            = 6

EOS_Thermal::evol_eos_name		= "Extable"
EOS_Thermal_Extable::rho_max		= 1e10
EOS_Thermal_Extable::temp_max		= 1000
EOS_Thermal_Extable::extend_ye		= "yes"

#EOS_Thermal_Table3d::eos_db_loc		= "/gpfs/work/pr62do/di25def2/initial/eos"
#EOS_Thermal_Table3d::eos_folder		= "DD2"
#EOS_Thermal_Table3d::eos_filename		= "DD2_DD2_hydro_30-Mar-2015.h5"

EOS_Thermal_Table3d::eos_db_loc   = "/home/astro/shared/projekt/EOS_Collection"
EOS_Thermal_Table3d::eos_folder   = "LS220"
EOS_Thermal_Table3d::eos_filename   = "LS220_hydro_09-Apr-2017.h5"

THC_Analysis::compute_eninf		= "yes"

# =============================================================================
# Spacetime evolution
# =============================================================================
ADMBase::evolution_method		= "ML_CCZ4"
ADMBase::lapse_evolution_method		= "ML_CCZ4"
ADMBase::shift_evolution_method		= "ML_CCZ4"
ADMBase::dtlapse_evolution_method		= "ML_CCZ4"
ADMBase::dtshift_evolution_method		= "ML_CCZ4"

ML_CCZ4::GammaShift                     = 0.5
ML_CCZ4::dampk1                 = 0.036  # ~ [0.02, 0.1]/M
ML_CCZ4::dampk2                 = 0.0
ML_CCZ4::harmonicN                      = 1.0    # 1+log
ML_CCZ4::harmonicF                      = 2.0    # 1+log
ML_CCZ4::ShiftGammaCoeff                = 0.75
ML_CCZ4::AlphaDriver            = 0.0
ML_CCZ4::BetaDriver                     = 0.3    # ~ [1, 2] / M (\eta)

ML_CCZ4::advectLapse            = 1
ML_CCZ4::advectShift            = 1

ML_CCZ4::MinimumLapse           = 1.0e-8
ML_CCZ4::conformalMethod                = 1      # 1 for W

ML_CCZ4::initial_boundary_condition          = "extrapolate-gammas"
ML_CCZ4::rhs_boundary_condition              = "NewRad"
Boundary::radpower                      = 2

ML_CCZ4::ML_log_confac_bound            = "none"
ML_CCZ4::ML_metric_bound                = "none"
ML_CCZ4::ML_Gamma_bound         = "none"
ML_CCZ4::ML_trace_curv_bound            = "none"
ML_CCZ4::ML_curv_bound          = "none"
ML_CCZ4::ML_lapse_bound         = "none"
ML_CCZ4::ML_dtlapse_bound               = "none"
ML_CCZ4::ML_shift_bound         = "none"
ML_CCZ4::ML_dtshift_bound               = "none"
ML_CCZ4::ML_Theta_bound         = "none"

ML_CCZ4::fdOrder                        = 4
THC_Core::fd_order                      = 4

Dissipation::order                      = 5
Dissipation::epsdis                     = 0.2
Dissipation::vars                       = "
ML_CCZ4::ML_log_confac
ML_CCZ4::ML_metric
ML_CCZ4::ML_trace_curv
ML_CCZ4::ML_curv
ML_CCZ4::ML_Gamma
ML_CCZ4::ML_lapse
ML_CCZ4::ML_shift
ML_CCZ4::ML_dtlapse
ML_CCZ4::ML_dtshift
ML_CCZ4::ML_Theta
"

# =============================================================================
# Analysis
# =============================================================================

# =============================================================================
# Checkpoint
# =============================================================================
CarpetIOHDF5::checkpoint		= "no"
CarpetIOHDF5::use_reflevels_from_checkpoint	= "yes"

IOUtil::checkpoint_on_terminate		= "no"
IOUtil::checkpoint_every		= 0
IOUtil::checkpoint_keep		= 0
IOUtil::recover			= "autoprobe"
IOUtil::checkpoint_dir		= "./checkpoint"
IOUtil::recover_dir			= "./checkpoint"

# =============================================================================
# Output
# =============================================================================
IOUtil::out_dir			= "./data/"
IOUtil::strict_io_parameter_check		= "yes"
IOUtil::parfile_write		= "copy"

CarpetIOBasic::outinfo_vars		= "
    Carpet::physical_time_per_hour
    SystemStatistics::maxrss_mb
    SystemStatistics::swap_used_mb
    ADMBase::lapse
    HydroBase::rho
"

CarpetIOScalar::outScalar_reductions		= "
    minimum maximum norm_inf norm1 norm2
"
CarpetIOScalar::outscalar_vars		= "
    thc_grsource_comparison::rhs_scon_reldiff
    thc_grsource_comparison::rhs_tau_reldiff
    ADMBase::lapse
    ADMBase::shift
    ADMBase::curv
    ADMBase::metric
    HydroBase::rho
    HydroBase::vel
    HydroBase::w_lorentz
    HydroBase::entropy
    HydroBase::eps
    HydroBase::press
    HydroBase::temperature
    HydroBase::Y_e
    ML_ADMConstraints::ML_Ham
    ML_ADMConstraints::ML_mom
    THC_Core::csound
    THC_Core::dens
    THC_Core::volform
"

CarpetIOASCII::out0D_vars		= "
    BNSTrackerGen::bns_positions
    Carpet::timing
    Volomnia::grid_volume
    Volomnia::cell_volume
"

CarpetIOASCII::out1d_vars		= "
    ADMBase::lapse
    ADMBase::shift
    ADMBase::curv
    ADMBase::metric
    HydroBase::rho
    HydroBase::vel
    HydroBase::w_lorentz
    HydroBase::eps
    HydroBase::press
    HydroBase::entropy
    HydroBase::temperature
    HydroBase::Y_e
    ML_ADMConstraints::ML_Ham
    ML_ADMConstraints::ML_mom
    THC_Analysis::eninf
    THC_Core::bitmask
    THC_Core::csound
    THC_Core::dens
    THC_Core::csound
    THC_Core::scon
    THC_Core::tau
    THC_Core::volform
"

CarpetIOHDF5::out2d_vars		= "
    thc_grsource_comparison::rhs_scon_reldiff
    thc_grsource_comparison::rhs_tau_reldiff
    thc_grsource_templates::rhs_scon_temp
    thc_grsource_templates::rhs_tau_temp
    thc_grsource_orig::rhs_scon_orig
    thc_grsource_orig::rhs_tau_orig
"

CarpetIOHDF5::out_vars		= "
    HydroBase::rho
    HydroBase::temperature
    HydroBase::Y_e
		HydroBase::eps
"

CarpetIOScalar::one_file_per_group		= "yes"
CarpetIOScalar::all_reductions_in_one_file	= "yes"
CarpetIOASCII::one_file_per_group		= "yes"
IO::out_single_precision		= "yes"

CarpetIOBasic::outinfo_every		= 128
CarpetIOScalar::outscalar_criterion		= "divisor"
CarpetIOASCII::out0d_criterion		= "divisor"
CarpetIOASCII::out0d_every		= 128
CarpetIOASCII::out1d_every		= 512
CarpetIOScalar::outscalar_every		= 64
CarpetIOHDF5::out2d_every		= 64
CarpetIOHDF5::out_every		= 0
